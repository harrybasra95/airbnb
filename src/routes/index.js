import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Login } from '../screens';

const AppNavigator = createStackNavigator(
     {
          login: {
               screen: Login
          }
     },
     {
          headerMode: 'none',
          navigationOptions: {
               headerVisible: false
          }
     }
);

export default createAppContainer(AppNavigator);
