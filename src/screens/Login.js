import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Button } from '../components';
class Login extends Component {
     render() {
          const {
               container,
               centerContainer,
               loginContainer,
               loginInnerContainer,
               topBar,
               textColor,
               logoBar,
               logo,
               headingTextView,
               headingText,
               socialButtonsView,
               loginButton,
               bottomBar,
               loginText
          } = styles.login;
          return (
               <View style={[container, centerContainer, loginContainer]}>
                    <View style={loginInnerContainer}>
                         <View style={topBar}>
                              <TouchableOpacity>
                                   <Text style={textColor}>Log in</Text>
                              </TouchableOpacity>
                         </View>
                         <View style={logoBar}>
                              <Image style={logo} source={images.logo_white} />
                         </View>
                         <View style={headingTextView}>
                              <Text style={headingText}>
                                   Welcome to Airbnb.
                              </Text>
                              <View
                                   style={[socialButtonsView, centerContainer]}
                              />
                              <Button buttonView={loginButton}>
                                   <Text style={[textColor, loginText]}>
                                        Continue with Facebook
                                   </Text>
                              </Button>
                              <Button buttonView={loginButton}>
                                   <Text style={[textColor, loginText]}>
                                        Create Account
                                   </Text>
                              </Button>
                         </View>
                         <View style={bottomBar}>
                              <TouchableOpacity>
                                   <Text style={[textColor]}>More Options</Text>
                              </TouchableOpacity>
                         </View>
                    </View>
               </View>
          );
     }
}

export default Login;
