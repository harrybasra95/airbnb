export default {
     container: {
          padding: 10,
          flex: 1
     },
     centerContainer: {
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1
     }
};
