import colors from '../lib/colors';

export default {
     loginContainer: {
          backgroundColor: colors.primary
     },
     loginInnerContainer: {
          height: '70%',
          width: '90%'
     },
     topBar: {
          flexDirection: 'row',
          justifyContent: 'flex-end',
          padding: 10
     },
     textColor: {
          color: colors.white
     },
     logoBar: {
          padding: 10,
          paddingTop: 20
     },
     logo: {
          width: 50,
          height: 50
     },
     headingTextView: {
          paddingTop: 40
     },
     headingText: {
          color: colors.white,
          fontSize: 28
     },
     loginButton: {
          padding: 25
     },
     loginText: {
          fontSize: 15
     },
     bottomBar: {
          marginTop: 30
     },
     socialButtonsView: {
          paddingBottom: 20
     }
};
