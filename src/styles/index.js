import basic from './basic';
import login from './login';
import button from './button';

export default {
     login: { ...basic, ...login },
     button: { ...basic, ...button },
     basic
};
