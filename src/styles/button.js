export default {
     outerView: {
          marginTop: 20
     },
     buttonView: {
          padding: 20,
          borderWidth: 2,
          borderColor: 'white',
          borderRadius: 100
     }
};
