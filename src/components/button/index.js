import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';

class Button extends Component {
     render() {
          const { buttonView, centerContainer, outerView } = styles.button;
          return (
               <View style={[outerView, this.props.style]}>
                    <TouchableOpacity>
                         <View
                              style={[
                                   buttonView,
                                   centerContainer,
                                   this.props.buttonView
                              ]}
                         >
                              {this.props.children}
                         </View>
                    </TouchableOpacity>
               </View>
          );
     }
}

export default Button;
