import styles from '../styles';
import colors from '../lib/colors';
import images from '../lib/images';
global.styles = styles;
global.colors = colors;
global.images = images;
