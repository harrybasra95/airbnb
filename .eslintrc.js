module.exports = {
     extends: ['eslint:recommended', 'plugin:react/recommended'],
     parser: 'babel-eslint',
     rules: {
          'no-use-before-define': ['error', { variables: false }],
          'linebreak-style': 0,
          indent: 0,
          'react/prop-types': 0
     },
     env: {
          browser: true,
          node: true,
          jasmine: true
     },
     globals: {
          store: true,
          persistor: true,
          styles: true,
          images: true
     }
};
